export default {
    mounted () {
        try {
            this.price = this.modifiedPacking[0].price
            this.packingId = this.modifiedPacking[0].id
        }
        catch (e) {
            console.log(e.message)
        }
    },
    data: () => ({
        starFilled: require('~/assets/img/starFilled.svg'),
        starEmpty: require('~/assets/img/starEmpty.svg'),
        packingId: null,
        packingValue: '',
        packingObj: {},
        maxCount: null,
        count: 1,
        price: null,
        starConfig: {
            rating: 0,
            style: {
                FullStarColor: '#F5BF00',
                EmptyStarColor: '#9c9c9c',
                StarWidth: 20,
                StarHeight: 20
            }
        }
    }),
    created () {
        this.starConfig.rating = this.product.ratings
    },
    computed: {
        brandSlug () {
            let array = this.product.href.brand.split('/')
            return array[array.length - 1]
        },
        isSeedsMethod: () => type => {
            return type.use_to === 'seeds_only' || type.use_to === 'seeds_and_dropshipping'
        },
        productSlug() {
            let array = this.product.href.details.split('/')
            return array[array.length - 1]
        },
        getBrandIcon () {
            return this.$store.getters['menu/getBrandIcon'](this.brandSlug)
        },
        getBrandName () {
            return this.$store.getters['menu/getBrandName'](this.brandSlug)
        },
        getCumulativeDiscount () {
            return this.$store.getters['checkout/getCumulativeDiscount']
        },
        isDiabledIncrement () {
            return this.count >= this.maxCount
        },
        isDiabledDecriment () {
            return this.count <= 1
        },
        promotionId () {
            return this.product.features.promotion_id
        },
        getDiscountByPromotionId () {
            return this.$store.getters['catalog/getDiscountByPromotionId'](this.promotionId)
        },
        discount () {
            const discountFromPromo = this.$store.getters['catalog/getDiscountByPromotionId'](this.promotionId)
            const discountFromProduct = this.product.discount
            const cumulativeDiscount = this.getCumulativeDiscount
            return Math.max(discountFromPromo, discountFromProduct, cumulativeDiscount) || 0
        },
        modifiedPacking () {
            let result = JSON.parse(JSON.stringify(this.product.packing))
            result = result.map(item => {
                let price = parseFloat(item.price)
                if(this.promotionId) {
                    item.price = Math.round(price - price * this.discount / 100)
                }
                item.price = Math.round(item.price)
                return item
            })
            return result
        },
        oldPrice() {
            let result = null
            let packing = this.product.packing.find(item => {
                return item.id === this.packingId
            })
            result = packing ? packing.price : null
            return Math.round(result)
        },
        modifiedCategories () {
            return this.product.features.categories.filter(item => {
                return item.cat_group !== 'Особенности'
            })
        }
    },
    methods: {
        quantityManagement(value) {
            if(value) {
                this.packingId = value.id
                this.maxCount = value.quantity
                this.price = value.price
                if(this.count > value.quantity) {
                    this.count = value.quantity
                }
            }
        },
        increment() {
            if(this.count < this.maxCount) {
                this.count += 1
            }
        },
        decriment() {
            if(this.count > 1) {
                this.count -= 1
            }
        }
    }
}
