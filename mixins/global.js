import { mapFields } from 'vuex-map-fields'
import { mapActions } from 'vuex';
export default {
    async fetch () {
        if(!this.isDataLoad) {
            await Promise.all([
                this.getData(),
                this.getPromotions(),
                this.getCheckoutData(),
                this.getBrands(),
                this.getCategories()
            ])
            this.isDataLoad = true
        }
    },
    computed: {
        ...mapFields('menu', [
            'isDataLoad'
        ]),
        routeName () {
            return this.$route.name
        }
    },
    methods: {
        ...mapActions('menu', [
            'getData'
        ]),
        ...mapActions('catalog', [
            'getPromotions'
        ]),
        ...mapActions('checkout', [
            'getCheckoutData'
        ]),
        ...mapActions('dropShipping', [
            'getBrands',
            'getCategories'
        ])
    }
}
