function addbits(s) {
    return (s.replace(/\s/g, '').match(/[+\-]?([0-9\.]+)/g) || [])
        .reduce((sum, value) => {
            return parseInt(sum) + parseInt(value);
        });
}

function modPackings(packingArray) {
    let result = packingArray.map((item) => {
        item.price = parseFloat(item.price.replace(/,/g, ''))
        item.price = Math.round(item.price)
        return item
    })
    return result.sort((a, b) => {
        const item1 = addbits(a.name)
        const item2 = addbits(b.name)
        if (item1 > item2) {
            return 1;
        }
        if (item1 < item2) {
            return -1;
        }
        return 0;
    })
}

const declensionDictionary = {
    days: ['день', 'дня', 'дней']
}

function declOfNum (number, wordType) {
    const words = declensionDictionary[wordType]
    return words[(number % 100 > 4 && number % 100 < 20) ? 2 : [2, 0, 1, 1, 1, 2][(number % 10 < 5) ? number % 10 : 5]]
}

export {
    modPackings,
    declOfNum
}
