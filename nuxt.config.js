export default {
    // Global page headers (https://go.nuxtjs.dev/config-head)
    head: {
        title: 'Green Market',
        meta: [
            { charset: 'utf-8' },
            { name: 'viewport', content: 'width=device-width, initial-scale=1' },
            { hid: 'description', name: 'description', content: '' },
            { hid: 'yandex-verification', name: 'yandex-verification', content: 'ac75d438c1a7c462' }
        ],
        link: [
            { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
        ],
        script: [
            {
                src: '//code.jivosite.com/widget/ZQQTAref6y',
                async: true
            },
            {
                src: 'https://zapcdn.space/zapret.js?message=1',
                async: true
            },
            {
                src: '/zapret.js'
            }
        ]
    },

    // Global CSS (https://go.nuxtjs.dev/config-css)
    css: [
        'reset-css',
        '@/assets/styles/base.scss'
    ],

    // Plugins to run before rendering page (https://go.nuxtjs.dev/config-plugins)
    plugins: [
        '~/plugins/path.js',
        '~/plugins/constants.js',
        { src: '~/plugins/notifications-ssr', mode: 'server' },
        { src: '~/plugins/notifications-client', mode: 'client' },
        { src: '~/plugins/star-rating.js', mode: 'client' },
        { src: '~/plugins/perfectScrollbar.js', mode: 'client' }
    ],

    // Auto import components (https://go.nuxtjs.dev/config-components)
    components: true,

    // Modules for dev and build (recommended) (https://go.nuxtjs.dev/config-modules)
    buildModules: [
        // https://go.nuxtjs.dev/eslint
    ],

    // Modules (https://go.nuxtjs.dev/config-modules)
    modules: [
        // https://go.nuxtjs.dev/axios
        '@nuxtjs/axios',
        '@nuxtjs/auth-next',
        'nuxt-vuex-localstorage',
        '@nuxtjs/gtm'
    ],
    gtm: {
        id: 'GTM-KTQ2C9L',
        enabled: true
    },
    auth: {
        strategies: {
            local: {
                token: {
                    property: 'access_token',
                },
                user: {
                    property: 'data',
                },
                endpoints: {
                    login: { url: '/auth/login', method: 'post' },
                    logout: { url: '/auth/logout', method: 'get' },
                    user: { url: '/profile', method: 'get'}
                }
            }
        }
    },

    loading: {
        color: '#124D25',
        height: '5px'
    },

    // Axios module configuration (https://go.nuxtjs.dev/config-axios)
    axios: {
        baseURL: 'https://mjcabinet.online/api/'
    },

    // Build Configuration (https://go.nuxtjs.dev/config-build)
    build: {
        transpile: ['vue-dynamic-star-rating', 'inputmask']
    }
}
