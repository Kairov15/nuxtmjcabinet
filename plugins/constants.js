export default (context, inject) => {
    inject('const', {
        WARN_SEEDS_MESSAGE: 'Внимание! Нельзя положить в корзину. Заказ на семена необходимо оформить отдельным заказом.',
        WARN_DROP_MESSAGE: 'Внимание! Оборудование можно заказать отдельно от семян.'
    })
}
