export default (context, inject) => {
    inject('baseUrlImg', 'https://mjcabinet.online/storage/')
    inject('domainRoot', 'https://mjcabinet.online/')
}
