export default [
    {
        id: Math.random(),
        caption: 'Тип упаковки',
        props: [
            {   
                id: Math.random(),
                value: '3',
                label: '3'
            },
            {
                id: Math.random(),
                value: '5+1',
                label: '5+1'
            },
            {
                id: Math.random(),
                value: '7',
                label: '7'
            },
            {
                id: Math.random(),
                value: '10+2',
                label: '10+2'
            },
            {
                id: Math.random(),
                value: '10',
                label: '10'
            }
        ]
    },
    {
        id: Math.random(),
        caption: 'Тип цветения',
        props: [
            {
                id: Math.random(),
                value: 'auto',
                label: 'Автоцветущие'
            },
            {
                id: Math.random(),
                value: 'foto',
                label: 'Фотопериодные'
            }
        ]
    },
    {
        id: Math.random(),
        caption: 'Пол',
        props: [
            {
                id: Math.random(),
                value: 'reg',
                label: 'Регулярные'
            },
            {
                id: Math.random(),
                value: 'fem',
                label: 'Феминизованные'
            }
        ]
    },
    {
        id: Math.random(),
        caption: 'Генетика',
        props: [
            {
                id: Math.random(),
                value: 'ind',
                label: 'Индика'
            },
            {
                id: Math.random(),
                value: 'sat',
                label: 'Сатива'
            }
        ]
    }
]