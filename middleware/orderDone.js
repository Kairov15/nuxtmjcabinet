export default function({store, redirect}) {
    if (!store.getters['checkout/checkoutDone']) {
        redirect('/')
    }
}
