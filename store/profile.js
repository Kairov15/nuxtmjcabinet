import { getField, updateField } from 'vuex-map-fields'

export const state = () => ({
    ordersList: [],
    availableCountries: [
        'Россия', 'Казахстан', 'Беларусь', 'Грузия', 'Таджикистан', 'Армения', 'Кыргызстан', 'Азербайджан', 'Молдова', 'Украина'
    ]
})

export const getters = {
    getField,
    orderListCount (state) {
        return state.ordersList.length
    }
}

export const mutations = {
    updateField,
    setOrders (state, payload) {
        state.ordersList = payload
    }
}

export const actions = {
    async getOrders({ commit }) {
        try {
            const data = await this.$axios.$get('/orders')
            commit('setOrders', data)
        } catch (e) {
            console.log(e)
        }
    }

}
