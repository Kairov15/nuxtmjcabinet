import { getField, updateField } from 'vuex-map-fields'

export const state = () => ({
    innerMenuPagesContent: []
})

export const getters = {
    getField,
    getInnerMenuPageContent: state => (slug) => {
        return state.innerMenuPagesContent.find(item => item.slug === slug)
    },
}

export const mutations = {
    updateField,
    setInnerMenuPagesContent (state, payload) {
        state.innerMenuPagesContent.push({
            slug: payload.slug,
            content: payload.content
        })
    },
}

export const actions = {
    async getMenuInnerPage ({ commit, getters }, slug) {
        const isset = getters.getInnerMenuPageContent(slug)
        try {
            if(!isset) {
                const data = await this.$axios.$get(`/menu/${slug}`)
                const content = data.data
                commit('setInnerMenuPagesContent', { content, slug })
            }
        } catch (e) {
            console.log(e)
        }
    }
}
