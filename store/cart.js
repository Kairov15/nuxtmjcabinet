import { getField, updateField } from 'vuex-map-fields'

export const state = () => ({
    cart: []
})

export const getters = {
    getField,
    total(state) {
        return state.cart.reduce((prev, current) => {
            return prev + current.count
        }, 0)
    },
    totalCartValue (state) {
        return state.cart.reduce((prev, next) => {
            return prev + next.count * parseFloat(next.price)
        },0)
    },
    isSeedsCart (state) {
        return !!state.cart.find(item => {
            return item.productType === 'seeds'
        })
    },
    isDropCart (state) {
        return !!state.cart.find(item => {
            return item.productType === 'drop'
        })
    },
    isDiabledCartProductIncrement: state => index => {
        const product = state.cart[index]
        return product.count >= product.maxCount
    },
    isDiabledCartProductDecriment: state => index => {
        const product = state.cart[index]
        return product.count <= 1
    },
    getCurrentCountPackingProductInCart: state => packingId => {
        let result = 0
        const findSameItemIndex = state.cart.findIndex(product => {
            return product.packingId === packingId
        })
        if(findSameItemIndex !== -1) {
            result = state.cart[findSameItemIndex].count
        }
        return result
    },
    getCurrentCountDropProduct: state => productId => {
        let result = 0
        const findSameItemIndex = state.cart.findIndex(product => {
            return product.product_id === productId
        })
        if(findSameItemIndex !== -1) {
            result = state.cart[findSameItemIndex].count
        }
        return result
    }
}

export const mutations = {
    updateField,
    vuexAddToCart (state, item) {
        const findSameItemIndex = state.cart.findIndex(product => {
            return product.packingId === item.packingId
        })
        if(findSameItemIndex !== -1) {
            state.cart[findSameItemIndex].count += item.count
        } else {
            state.cart.push(item)
        }
    },
    vuexDropAddToCart (state, item) {
        const findSameItemIndex = state.cart.findIndex(product => {
            return product.product_id === item.product_id
        })
        if(findSameItemIndex !== -1) {
            state.cart[findSameItemIndex].count += item.count
        } else {
            state.cart.push(item)
        }
    },
    removeItem (state, index) {
        state.cart.splice(index, 1)
    },
    incrementCartProduct (state, index) {
        const product = state.cart[index]
        if(product.count < product.maxCount) {
            product.count += 1
        }
    },
    decrimentCartProduct (state, index) {
        const product = state.cart[index]
        if(product.count > 1) {
            product.count -= 1
        }
    },
    clearCart (state) {
        state.cart = []
    },
    syncWithStore (state, payload) {
        console.log(payload)
        if (payload) {
            payload.forEach(item => {
                const index = state.cart.findIndex(cartItem => {
                    return cartItem.packingId === item.id
                })
                if(index !== -1) {
                    if(item.quantity <= 0) {
                        state.cart.splice(index, 1)
                    } else {
                        state.cart[index].count = item.quantity
                    }
                }
            })
        }
    }
}

export const actions = {
    vuexAddToCart ({ state, commit, rootState }, item) {
        commit('vuexAddToCart', item)
        commit('localStorage/setCart', state.cart, { root: true })
    },
    vuexDropAddToCart ({ state, commit, rootState }, item) {
        commit('vuexDropAddToCart', item)
        commit('localStorage/setCart', state.cart, { root: true })
    }
}
