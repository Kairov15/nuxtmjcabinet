import { getField, updateField } from 'vuex-map-fields'

export const state = () => ({
    reviewsCount: {},
    reviews: {},
    reviewsOnMain: {}
})

export const getters = {
    getField,
}

export const mutations = {
    updateField,
    setReviewsCount(state, reviewsCount) {
        state.reviewsCount = reviewsCount
    },
    setReviews(state, reviews) {
        state.reviews = reviews
        state.reviewsOnMain = reviews
    },
    pushReviews(state, payload) {
        state.reviews.data.push(...payload.content.data)
        state.reviews.links = payload.content.links
        state.reviews.meta = payload.content.meta
    }
}

export const actions = {
    async getReviewsCount ({ commit }) {
        try {
            const reviewsCount = await this.$axios.$get('/reviews_count')
            commit('setReviewsCount', reviewsCount)
        } catch (e) {
            console.log(e)
        }
    },
    async getReviews ({ commit }, payload) {
        try {
            const reviews = await this.$axios.$get(`/reviews?paginate=10&page=${payload.page}`)
            commit('setReviews', reviews)
        } catch (e) {
            console.log(e)
        }
    },
    async getInfiniteReviews({ commit, state }) {
        const currentPage = state.reviews.meta.current_page
        try {
            const data = await this.$axios.$get(`/reviews?paginate=10&page=${currentPage + 1}`)
            commit('pushReviews', {
                content: data,
            })
        } catch (e) {
            console.log(e)
        }
    }
}
