import { getField, updateField } from 'vuex-map-fields'

export const state = () => ({
    showMenu: false,
    showLoader: false,
    showHooperGallery: false,
    menu: [],
    catalog: {
        categories: [],
        brands: []
    },
    isDataLoad: false,
    windowWidth: null
})

export const getters = {
    getField,
    getInnerCatalogPageTitle: state => (slug) => {
        let result
        for (const key in state.catalog) {
            if (Object.prototype.hasOwnProperty.call(state.catalog, key)) {
                if (!state.catalog[key].find(item => item.slug === slug)) { continue }
                result = state.catalog[key].find(item => item.slug === slug)
            }
        }
        return result ? result.name : result
    },
    getBrandIcon: state => (slug) => {
        const slugMod = slug.replace('%20', ' ')
        const result = state.catalog.brands.find(item => item.slug === slugMod)
        return result ? result.icon : result
    },
    getBrandName: state => (slug) => {
        const slugMod = slug.replace('%20', ' ')
        const result = state.catalog.brands.find(item => item.slug === slugMod)
        return result ? result.name : result
    },
    getBrandDescription: state => (slug) => {
        const result = state.catalog.brands.find(item => item.slug === slug)
        return result ? result.description : result
    }
}

export const mutations = {
    updateField,
    setMenu (state, menu) {
        state.menu = menu
    },
    setCategories (state, categories) {
        state.catalog.categories = categories
    },
    setBrands (state, brands) {
        state.catalog.brands = brands
    }
}

export const actions = {
    async getMenu ({ commit }) {
        const data = await this.$axios.$get('/menu')
        const menu = data.data
        commit('setMenu', menu)
    },
    async getCategories ({ commit }) {
        const data = await this.$axios.$get('/semena/categories')
        const categories = data.data
        commit('setCategories', categories)
    },
    async getBrands ({ commit }) {
        const data = await this.$axios.$get('/semena/brands')
        const brands = data.data
        commit('setBrands', brands)
    },
    async getData ({ dispatch }) {
        await Promise.all([
            dispatch('getMenu'),
            dispatch('getCategories'),
            dispatch('getBrands')
        ])
    }
}
