export const state = () => ({
    cart: []
})

export const mutations = {
    setCart (state, payload) {
        state.cart = payload
    }
}
