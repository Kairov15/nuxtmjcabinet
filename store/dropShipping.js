import { getField, updateField } from 'vuex-map-fields'

export const state = () => ({
    categories: [],
    brands: [],
    innerDropPage: [],
    activeView: 'list'
})

export const getters = {
    getField,
    getInnerDropPageIndex: state => (slug) => {
        const result = state.innerDropPage.findIndex(item => item.slug === slug)
        return result
    },
    getInnerDropPageTitle: state => (payload) => {
        let result = state[payload.targetArray].find(item => {
            return item.slug === payload.slug
        })
        return result ? result.name : result
    },
    getInnerDropPage: state => (slug) => {
        const result = state.innerDropPage.find(item => item.slug === slug)
        return result ? result.content : result
    },
    getCurrentMetaPageBySlug: state => slug => {
        const result = state.innerDropPage.find(item => item.slug === slug)
        return result ? {
            current_page: result.content.meta.current_page,
            last_page: result.content.meta.last_page,
        } : {
            current_page: 1,
            last_page: 1,
        }
    },
}

export const mutations = {
    updateField,
    setCategories (state, categories) {
        state.categories = categories
    },
    setBrands (state, brands) {
        state.brands = brands
    },
    setInnerDropPage (state, payload) {
        if(payload.categoryIndex === -1) {
            state.innerDropPage.push({
                slug: payload.slug,
                content: payload.content
            })
        } else {
            state.innerDropPage[payload.categoryIndex].content.data = payload.content.data
            state.innerDropPage[payload.categoryIndex].content.links = payload.content.links
            state.innerDropPage[payload.categoryIndex].content.meta = payload.content.meta
        }
    },
    pushInnerDropPage(state, payload) {
        state.innerDropPage[payload.categoryIndex].content.data.push(...payload.content.data)
        state.innerDropPage[payload.categoryIndex].content.links = payload.content.links
        state.innerDropPage[payload.categoryIndex].content.meta = payload.content.meta
    }
}

export const actions = {
    async getCategories ({ commit }) {
        try {
            const data = await this.$axios.$get('/drop/categories')
            const categories = data.data
            commit('setCategories', categories)
        } catch (e) {
            console.log(e)
        }
    },
    async getCategory({ commit, getters }, params) {
        try {
            const data = await this.$axios.$get(`/drop/category/${params.slug}?paginate=8&page=${params.page}`)
            const content = data
            const categoryIndex = getters.getInnerDropPageIndex(params.slug)
            commit('setInnerDropPage', { content, slug: params.slug, categoryIndex })
        } catch (e) {
            console.log(e)
        }
    },
    async getBrands ({ commit }) {
        try {
            const data = await this.$axios.$get('/drop/brands')
            const brands = data.data
            commit('setBrands', brands)
        } catch (e) {
            console.log(e)
        }
    },
    async getBrand ({ commit, getters }, params) {
        try {
            const data = await this.$axios.$get(`/drop/brand/${params.slug}?paginate=8&page=${params.page}`)
            const content = data
            const categoryIndex = getters.getInnerDropPageIndex(params.slug)
            commit('setInnerDropPage', { content, slug: params.slug, categoryIndex })
        } catch (e) {
            console.log(e)
        }
    },
    async getInfiniteCategory({ commit, getters }, params) {
        const currentPage = getters.getCurrentMetaPageBySlug(params.slug).current_page
        const data = await this.$axios.$get(`${params.url}${params.slug}?paginate=8&page=${currentPage + 1}`)
        const categoryIndex = getters.getInnerDropPageIndex(params.slug)
        commit('pushInnerDropPage', {
            content: data,
            categoryIndex
        })
    },
}
