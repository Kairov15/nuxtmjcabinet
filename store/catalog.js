import { getField, updateField } from 'vuex-map-fields'
import { modPackings } from '~/helpers/helpers'

export const state = () => ({
    promotions: [],
    promotionsDetail: [],
    promotionsDetailKostyl: [],
    hitOnMain: [],
    newOnMain: [],
    innerCatalogPage: [],
    filterParams: [],
    featuresList: [],
    searchResult: {},
    searchQuery: '',
    activeView: 'list',
})

export const getters = {
    getField,
    getInnerCatalogPage: state => (slug) => {
        const result = state.innerCatalogPage.find(item => item.slug === slug)
        return result ? result.content : result
    },
    getInnerCatalogPageIndex: state => (slug) => {
        const result = state.innerCatalogPage.findIndex(item => item.slug === slug)
        return result
    },
    getDiscountByPromotionId: state => (id) => {
        const result = state.promotionsDetailKostyl.find(promotion => parseInt(promotion.id) === parseInt(id))
        return result ? result.percentage : result
    },
    getDetailPromotion: state => slug => {
        return state.promotionsDetail.find(promotion => {
            return promotion.slug === slug
        })
    },
    getDetailPromotionIndex: state => slug => {
        return state.promotionsDetail.findIndex(promotion => promotion.slug === slug)
    },
    getCurrentMetaPageBySlug: state => slug => {
        const result = state.innerCatalogPage.find(item => item.slug === slug)
        return result ? {
            current_page: result.content.meta.current_page,
            last_page: result.content.meta.last_page,
        } : {
            current_page: 1,
            last_page: 1,
        }
    },
    getCurrentMetaPromotionPageBySlug: state => slug => {
        const result = state.promotionsDetail.find(item => item.slug === slug)
        return result ? {
            current_page: +result.products.meta.current_page,
            last_page: +result.products.meta.last_page,
        } : {
            current_page: 1,
            last_page: 1,
        }
    },
    getDetailPromotionIndexKostyl: state => slug => {
        return state.promotionsDetailKostyl.findIndex(promotion => promotion.slug === slug)
    },
    getCurrentFeatureId: state => slug => {
        const item = state.featuresList.find(item => {
            return item.slug === slug
        })
        return item.value
    }
}

export const mutations = {
    updateField,
    setPromotions (state, promotions) {
        state.promotions = promotions
    },
    setPromotion (state, payload) {
        if (payload.promotion.products.data.length) {
            payload.promotion.products.data.map((item) => {
                modPackings(item.packing)
            })
        }
        state.promotions.forEach((promo) => {
            const priority = promo.priority
            if (promo.slug === payload.promotion.slug) {
                payload.promotion.priority = priority
            }
        })
        if(payload.index === -1) {
            state.promotionsDetail.push(payload.promotion)
        } else {
            state.promotionsDetail[payload.index].products.data = payload.promotion.products.data
            state.promotionsDetail[payload.index].products.links = payload.promotion.products.links
            state.promotionsDetail[payload.index].products.meta = payload.promotion.products.meta
        }
    },
    setPromotionKosltyl (state, payload) {
        if (payload.promotion.products.data.length) {
            payload.promotion.products.data.map((item) => {
                modPackings(item.packing)
            })
        }
        state.promotions.forEach((promo) => {
            const priority = promo.priority
            const image = promo.image
            if (promo.slug === payload.promotion.slug) {
                payload.promotion.priority = priority
                payload.promotion.image = image
            }
        })
        if(payload.index === -1) {
            state.promotionsDetailKostyl.push(payload.promotion)
        } else {
            state.promotionsDetailKostyl.splice(payload.index, 1)
            state.promotionsDetailKostyl[payload.index] = payload.promotion
        }
    },
    setInnerCategoryPage (state, payload) {
        payload.content.data.map((item) => {
            modPackings(item.packing)
        })
        if(payload.categoryIndex === -1) {
            state.innerCatalogPage.push({
                slug: payload.slug,
                content: payload.content
            })
        } else {
            state.innerCatalogPage[payload.categoryIndex].content.data = payload.content.data
            state.innerCatalogPage[payload.categoryIndex].content.links = payload.content.links
            state.innerCatalogPage[payload.categoryIndex].content.meta = payload.content.meta
        }
    },
    setSearchResult (state, payload) {
        payload.data.map((item) => {
            modPackings(item.packing)
        })
        state.searchResult = payload
    },
    setFilterParams (state, filterParams) {
        let result = []
        for (const key in filterParams) {
            let attributeGroup = []
            let props = []
            let filterParamsMod = []
            if(!Array.isArray(filterParams[key])) {
                filterParamsMod.push(filterParams[key])
            } else {
                filterParamsMod = filterParams[key]
            }

            attributeGroup = filterParamsMod.map(group => {
                props = group['values'].map(prop => {
                    return {
                        id: `${group.slug}-${prop.id}`,
                        label: prop.name,
                        value: prop.id.toString(),
                        slug: prop.slug
                    }
                })
                return {
                    caption: group.name,
                    props
                }
            })
            result.push({
                attributeType: key,
                attributeGroup
            })
        }
        const categoryItems = result.find(filterParam => {
            return filterParam.attributeType === 'category_items'
        })
        const featureList = categoryItems.attributeGroup.reduce((prev, next) => {
            return [...prev, ...next.props]
        }, [])
        state.featuresList = featureList
        state.filterParams = result
    },
    setHitOnMain (state, hit) {
        hit.map((item) => {
            modPackings(item.packing)
        })
        state.hitOnMain = hit
    },
    setNewOnMain (state, newProducts) {
        newProducts.map((item) => {
            modPackings(item.packing)
        })
        state.newOnMain = newProducts
    },
    pushInnerCategoryPage(state, payload) {
        payload.content.data.map((item) => {
            modPackings(item.packing)
        })
        state.innerCatalogPage[payload.categoryIndex].content.data.push(...payload.content.data)
        state.innerCatalogPage[payload.categoryIndex].content.links = payload.content.links
        state.innerCatalogPage[payload.categoryIndex].content.meta = payload.content.meta
    },
    pushInnerDetailPromotion (state, payload) {
        if (payload.content.products.data.length) {
            payload.content.products.data.map((item) => {
                modPackings(item.packing)
            })
        }
        state.promotionsDetail[payload.promotionIndex].products.data.push(...payload.content.products.data)
        state.promotionsDetail[payload.promotionIndex].products.links = payload.content.products.links
        state.promotionsDetail[payload.promotionIndex].products.meta = payload.content.products.meta
    }
}

export const actions = {
    async getPromotions({ commit, getters }) {
        const data = await this.$axios.$get('/semena/promotions')
        const promotions = await data.data
        commit('setPromotions', promotions)
        const promises = promotions.map(async (promotion) => {
            const data = await this.$axios.$get(`/semena/promotions/${promotion.slug}?paginate=20&page=1`)
            const promotionItem = data.data
            const promotionIndex = getters.getDetailPromotionIndexKostyl(promotionItem.slug)
            commit('setPromotionKosltyl', { promotion: promotionItem, index: promotionIndex })
        })
        await Promise.all(promises)
    },
    async getPromotion ({ commit, getters }, payload) {
        try {
            const data = await this.$axios.$get(`/semena/promotions/${payload.slug}?paginate=8&page=${payload.page}`)
            const promotionItem = data.data
            const promotionIndex = getters.getDetailPromotionIndex(promotionItem.slug)
            commit('setPromotion', { promotion: promotionItem, index: promotionIndex })
        } catch (e) {
            console.log(e)
        }
    },
    async getCategory({ commit, getters }, params) {
        const data = await this.$axios.$get(`/semena/category/${params.slug}?paginate=8&page=${params.page}`)
        const content = data
        const categoryIndex = getters.getInnerCatalogPageIndex(params.slug)
        commit('setInnerCategoryPage', { content, slug: params.slug, categoryIndex })
    },
    async getInfiniteCategory({ commit, getters }, params) {
        const currentPage = getters.getCurrentMetaPageBySlug(params.slug).current_page
        const data = await this.$axios.$get(`/semena/category/${params.slug}?paginate=8&page=${currentPage + 1}`)
        const categoryIndex = getters.getInnerCatalogPageIndex(params.slug)
        commit('pushInnerCategoryPage', {
            content: data,
            categoryIndex
        })
    },
    async getInfiniteFilterProducts({ commit, getters }, params) {
        const currentPage = getters.getCurrentMetaPageBySlug(params.slug).current_page
        const data = await this.$axios.$post(`/semena/product?paginate=8&page=${currentPage + 1}`, {
            array: params.filterValue,
            brand: params.brand || null
        })
        const categoryIndex = getters.getInnerCatalogPageIndex(params.slug)
        commit('pushInnerCategoryPage', {
            content: data,
            categoryIndex
        })
    },
    async getSearchResult({ commit }, payload) {
        try {
            const data = await this.$axios.$get(`/semena/search/${payload.query}?paginate=8&page=${payload.page}`)
            commit('setSearchResult', data)
        } catch (e) {
            console.log(e)
        }
    },
    async getHitOnMain({ commit, state }) {
        const isset = state.hitOnMain.length
        try {
            if(!isset) {
                const data = await this.$axios.$get('/semena/xit?paginate=10&page=1')
                commit('setHitOnMain', data.data)
            }
        } catch (e) {
            console.log(e)
        }
    },
    async getNewOnMain({ commit, state }) {
        const isset = state.newOnMain.length
        try {
            if(!isset) {
                const data = await this.$axios.$get('/semena/new?paginate=10&page=1')
                commit('setNewOnMain', data.data)
            }
        } catch (e) {
            console.log(e)
        }
    },
    async getBrand({ commit, getters }, params) {
        const data = await this.$axios.$get(`/semena/brand/${params.slug}?paginate=8&page=${params.page}`)
        const content = data
        const categoryIndex = getters.getInnerCatalogPageIndex(params.slug)
        commit('setInnerCategoryPage', { content, slug: params.slug, categoryIndex })
    },
    async getInfiniteBrand({ commit, getters }, params) {
        const currentPage = getters.getCurrentMetaPageBySlug(params.slug).current_page
        const data = await this.$axios.$get(`/semena/brand/${params.slug}?paginate=8&page=${currentPage + 1}`)
        const content = data
        const categoryIndex = getters.getInnerCatalogPageIndex(params.slug)
        commit('pushInnerCategoryPage', {
            content,
            categoryIndex
        })
    },
    async getFilterParams({commit}) {
        const data = await this.$axios.$get('/semena/filter/list')
        commit('setFilterParams', data.data)
    },
    async filterProducts ({getters, commit}, params) {
        const data = await this.$axios.$post(`/semena/product?paginate=8&page=${params.page}`, {
            array: params.filterValue,
            brand: params.brand || null
        })
        const content = data
        const categoryIndex = getters.getInnerCatalogPageIndex(params.slug)
        commit('setInnerCategoryPage', { content, slug: params.slug, categoryIndex })
    },
    async getInfiniteInnerDetailPromotion({ commit, getters }, params) {
        const currentPage = getters.getCurrentMetaPromotionPageBySlug(params.slug).current_page
        const data = await this.$axios.$get(`/semena/promotions/${params.slug}?paginate=8&page=${currentPage + 1}`)
        const promotionIndex = getters.getDetailPromotionIndex(params.slug)
        commit('pushInnerDetailPromotion', {
            content: data.data,
            promotionIndex
        })
    },
}
