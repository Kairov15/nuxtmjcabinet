import { getField, updateField } from 'vuex-map-fields'

export const state = () => ({
    news: {},
    newsOnMain: {},
    newsDetail: [],
    articles: {},
    articlesOnMain: {},
    articlesDetail: []
})

export const getters = {
    getField,
    issetNewsList (state) {
        return Object.keys(state.news).length
    },
    issetNewsOnMain (state) {
        return Object.keys(state.newsOnMain).length
    },
    issetArticlesList (state) {
        return Object.keys(state.articles).length
    },
    issetArticlesOnMain (state) {
        return Object.keys(state.articlesOnMain).length
    },
    getNews: state => (slug) => {
        return state.newsDetail.find(item => item.slug === slug)
    },
    getNewsIndex: state => (slug) => {
        return state.newsDetail.findIndex(item => item.slug === slug)
    },
    getArticle: state => (slug) => {
        return state.articlesDetail.find(item => item.slug === slug)
    },
    getArticleIndex: state => (slug) => {
        return state.articlesDetail.findIndex(item => item.slug === slug)
    },
}

export const mutations = {
    updateField,
    setNewsList (state, news) {
        state.news = news
    },
    setNewsOnMain (state, news) {
        state.newsOnMain = news
    },
    setNews (state, payload) {
        if(payload.newsIndex === -1) {
            state.newsDetail.push(payload.news)
        } else {
            state.newsDetail.splice(payload.newsIndex, 1)
            state.newsDetail[payload.newsIndex] = payload.news
        }
    },
    setArticle (state, payload) {
        if(payload.articleIndex === -1) {
            state.articlesDetail.push(payload.article)
        } else {
            state.articlesDetail.splice(payload.articleIndex, 1)
            state.articlesDetail[payload.articleIndex] = payload.article
        }
    },
    setArticlesList (state, articles) {
        state.articles = articles
    },
    setArticlesOnMain (state, articles) {
        state.articlesOnMain = articles
    }
}

export const actions = {
    async getNewsList ({ commit, getters }, params) {
        try {
            const news = await this.$axios.$get(`/menu/news?paginate=10&page=${params.page}`)
            commit('setNewsList', news)
        } catch (e) {
            console.log(e)
        }
    },
    async getNewsOnMain ({ commit, getters }) {
        const isset = getters.issetNewsOnMain
        try {
            if(!isset) {
                const news = await this.$axios.$get(`/menu/news?paginate=3&page=1`)
                commit('setNewsOnMain', news)
            }
        } catch (e) {
            console.log(e)
        }
    },
    async getOneNews ({ commit, getters }, slug) {
        try {
            const isset = getters.getNews(slug)
            if(!isset) {
                const news = await this.$axios.$get(`/menu/news/${slug}`)
                const newsIndex = getters.getNewsIndex(slug)
                commit('setNews', {
                    news: news.data,
                    newsIndex: newsIndex
                })
            }
        } catch (e) {
            console.log(e)
        }
    },
    async getOneArticle ({ commit, getters }, slug) {
        try {
            const isset = getters.getArticle(slug)
            if(!isset) {
                const article = await this.$axios.$get(`/menu/article/${slug}`)
                const articleIndex = getters.getArticleIndex(slug)
                commit('setArticle', {
                    article: article.data,
                    articleIndex: articleIndex
                })
            }
        } catch (e) {
            console.log(e)
        }
    },
    async getArticlesList ({ commit }, params) {
        try {
            const articles = await this.$axios.$get(`/menu/article?paginate=10&page=${params.page}`)
            commit('setArticlesList', articles)
        } catch (e) {
            console.log(e)
        }
    },
    async getArticlesOnMain ({ commit, getters }) {
        const isset = getters.issetArticlesOnMain
        try {
            if(!isset) {
                const articles = await this.$axios.$get(`/menu/article?paginate=3&page=1`)
                commit('setArticlesOnMain', articles)
            }
        } catch (e) {
            console.log(e)
        }
    }
}
