import { getField, updateField } from 'vuex-map-fields'

export const state = () => ({
    product: {},
    dropProduct: {}
})

export const getters = {
    getField
}

export const mutations = {
    updateField,
    setProduct (state, product) {
        product.packing.map((item) => {
            item.price = parseFloat(item.price.replace(/,/g, ''))
            return item
        })
        state.product = product
    },
    setDropProduct (state, dropProduct) {
        state.dropProduct = dropProduct
    },
}

export const actions = {
    async getProduct({ commit }, slug) {
        try {
            const data = await this.$axios.$get(`/semena/product/${slug}`)
            const product = data.data
            commit('setProduct', product)
        } catch (e) {
            console.log(e)
        }
    },
    async getDropProduct({ commit }, slug) {
        try {
            const data = await this.$axios.$get(`/drop/product/${slug}`)
            const dropProduct = data.data
            commit('setDropProduct', dropProduct)
        } catch (e) {
            console.log(e)
        }
    },
}
