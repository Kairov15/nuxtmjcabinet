import { getField, updateField } from 'vuex-map-fields'

export const state = () => ({
    paymentTypes: [],
    shippingTypes: [],
    checkoutResponse: {},
    cumulativeDiscount: null
})

export const getters = {
    getField,
    getPaymentAccount: state => id => {
        let result = ''
        const find = state.paymentTypes.find(item => {
            return item.id === id
        })
        if(find) {
            result = find.account
        }
        return result
    },
    checkoutDone (state) {
        return Object.keys(state.checkoutResponse).length
    },
    getCumulativeDiscount (state) {
        return state.cumulativeDiscount && state.cumulativeDiscount.hasOwnProperty('cumulation') ? state.cumulativeDiscount.cumulation : 0
    }
}

export const mutations = {
    updateField,
    setPaymentTypes (state, data) {
        state.paymentTypes = data
    },
    setShippingTypes (state, data) {
        state.shippingTypes = data
    },
    setCheckoutResponse (state, data) {
        state.checkoutResponse = data
    },
    setСumulativeDiscount (state, data) {
        state.cumulativeDiscount = data
    }
}

export const actions = {
    async getPaymentTypes ({ commit }) {
        const data = await this.$axios.$get('/payment_methods')
        commit('setPaymentTypes', data.data)
    },
    async getShippingTypes ({ commit }) {
        const data = await this.$axios.$get('/shipping_methods')
        commit('setShippingTypes', data.data)
    },
    async getLocationShippingTypes ({ commit }, companies) {
        // console.log(companies)
        const data = await this.$axios.$get('/shipping_methods')
        let result = []
        for (let item of data.data) {
            // var avail_countries = item.avail_countries ? item.avail_countries.split(',') : []
            // console.log(item)
            // console.log(item.avail_countries)
            for (let company of companies) {
                if (item.alias == company.alias && item.delivery_type == company.delivery_type && item.avail_countries.indexOf(company.country_code) != -1) {
                    result.push(item)
                    break
                }
            }
            /*if (companies.indexOf(item.alias) != -1) {
                result.push(item)
            }*/
        }
        // console.log(result)
        return result
    },
    async getCheckoutData ({ dispatch }) {
        await Promise.all([
            dispatch('getPaymentTypes'),
            dispatch('getShippingTypes'),
            dispatch('getСumulativeDiscount')
        ])
    },
    async vuexCheckout ({commit}, payload) {
        try {
            const data = await this.$axios.$post('/checkout', payload)
            if(data.success) {
                commit('setCheckoutResponse', data.success)
            }
            return data
        } catch (e) {
            return e
        }
    },
    async getСumulativeDiscount ({ commit }) {
        try {
            const data = await this.$axios.$post('/checkout/discount_percent')
            commit('setСumulativeDiscount', data)
        } catch (e) {
            console.log(e)
        }
    },
    async getLocations ({ commit }, request) {
        try {
            const data = await this.$axios.$get(`/shipping/search_location?q=${request.q}&country=${request.country}`)
            // console.log(data)
            return data.data
        } catch (e) {
            console.log(e)
            return null
        }
    },
    async getPoints ({ commit }, location) {
        try {
            const data = await this.$axios.$get(`/shipping/get_points?city=${location.city}&country_code=${location.country_code}&common_region_code=${location.common_region_code}`)
            // console.log(data)
            return data
        } catch (e) {
            console.log(e)
            return null
        }
    },
    async calculateDelivery({ commit }, request) {
        try {
            const data = await this.$axios.$get(`/shipping/calculate_delivery?delivery_company=${request.shipping_type.alias}&city=${request.location.city}&common_region_code=${request.location.common_region_code}&country_code=${request.location.country_code}&tariff_code=${request.shipping_type.tariff_code}&delivery_type=${request.shipping_type.delivery_type}&total_sum=${request.total_sum}`)
            console.log(data)
            if (data.success) {
                return data.calculation
            }
            else {
                return {price: 0, days: null}
            }
        } catch (e) {
            console.log(e)
            return null
        }
    },
    async calcDeliveryDrop({ commit }, request) {
        try {
            const data = await this.$axios.$post('/shipping/calc_delivery_drop', request);
            console.log(data)
            if (data.success) {
                return data.calculation
            }
            else {
                return {price: 0, days: null}
            }
        } catch (e) {
            console.log(e)
            return null
        }
    }
}
